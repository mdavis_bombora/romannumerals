namespace RomanNumerals
{
    public interface IRomanNumeralParser
    {
        int Parse(string romanNumeral);
    }
}