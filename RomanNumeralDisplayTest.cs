using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace RomanNumerals
{
    public class RomanNumeralDisplayTest
    {
        [Test]
        public void Displays_parsed_number()
        {
            var parserMock = new Mock<IRomanNumeralParser>();
            parserMock.Setup(x => x.Parse("asdlkajas")).Returns(5);
            var displayer = new RomanNumeralDisplay(parserMock.Object);
            displayer.ReadNumber("asdlkajas").Should().Be(5);
        }
    }
}