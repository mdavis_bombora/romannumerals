namespace RomanNumerals
{
    public class RomanNumeralDisplay
    {
        private readonly IRomanNumeralParser _parser;

        public RomanNumeralDisplay(IRomanNumeralParser parser)
        {
            _parser = parser;
        }

        public int ReadNumber(string romanNumeral)
        {
            return _parser.Parse(romanNumeral);
        }
    }
}