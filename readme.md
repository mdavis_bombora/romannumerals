# Roman Numerals

## Problem Description

The Romans were a clever bunch. They conquered most of Europe and ruled it for hundreds of years. They invented concrete and straight roads. One thing they never discovered though was the number zero. This made writing and dating extensive histories of their exploits slightly more challenging, but the system of numbers they came up with is still in use today. For example the BBC uses Roman numerals to date their programmes.

The Romans wrote numbers using letters : I, V, X, L, C, D, M. (notice these letters have lots of straight lines and are hence easy to hack into stone tablets)

Write an application that can take in a Roman numeral number string and return that as an integer number.

**Notes:**

Each character in a Roman Numeral has a specific value.

- I = 1
- V = 5
- X = 10
- L = 50
- C = 100
- D = 500
- M = 1000

Depending on the placement of the letter in the numeral string the represented number could be additive or negative. When adding the higher number goes first and when subtracting the lower numbers goes first. 

- II = 2
- IV = 4
- VI = 3

Sequential additive numbers are allowed where as sequential negative numbers are not allowed.

- VII = 7
- XCIV = 94
- IIV = Not allowed

There is no representation of zero but a lack of a number should constitute as zero.