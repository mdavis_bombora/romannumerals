using System;
using FluentAssertions;
using NUnit.Framework;

namespace RomanNumerals
{
    class RomanNumeralParserTest
    {
        [Test]
        public void Parses_I_as_1()
        {
            new RomanNumeralParser().Parse("I").Should().Be(1);
        }
        
        [Test]
        public void Parses_V_as_5()
        {
            new RomanNumeralParser().Parse("V").Should().Be(5);
        }
    }

    internal class RomanNumeralParser : IRomanNumeralParser
    {
        public int Parse(string romanNumeral)
        {
            switch (romanNumeral)
            {
                case "I": return 1;
                case "V": return 5;
            }

            throw new FormatException("");
        }
    }
}